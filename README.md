# Robot Framework Quickstart

I intend to make this repository a **quickstart** for testing in Robot Framework regardless of testing type. So when starting a new project I **don't have to start from the scratch** but have the structure prepared with some example tests and definitions and all of the needed packages within the requirements file. 


# Testing types

This repository meant to be robust so I would like to **implement all the testing types possible to test with Robot Framework**. So when I would clone the repository, I could just erase what is not needed and focus on polishing the rest to the actual project. 

## Web testing

The classic web testing using the **Selenium** library and xpaths. 

## API testing

The REST API testing with the scryfall API as the example using **Requests** library. 

## Database testing

The db testing using SQL queries with **DatabaseLibrary** and the mysql as the db of choice. 

# Using the repository

Right now its meant to be used with virtualenv with packages installed with provided requirements file, but **I want to add a Dockerfile** so it can be also used with container. 

TBU. 