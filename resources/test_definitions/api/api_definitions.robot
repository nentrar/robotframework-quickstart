*** Settings ***
Resource   ../../../settings/settings.resource

*** Keywords ***
Given API is available at "${endpoint}" endpoint
	${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
	${response}=    GET On Session    my_session    ${endpoint}    headers=${headers}
    Should Be Equal As Integers    ${response.status_code}    200
    Log    API status code: ${response.status_code}    

When user searches for set "${set_name}"
    ${response}=    GET On Session    my_session    ${SETS_ENDPOINT}
    ${sets}=        Set Variable    ${response.json()['data']}
    ${matching_set}=    Set Variable    ${EMPTY}
    FOR    ${set}    IN    @{sets}
        Log    ${set}
        ${name}=    Set Variable    ${set['name']}
        Log    ${name}
        ${result}=  Run Keyword And Return Status    Should Be Equal As Strings    ${set_name.lower()}    ${name.lower()}
        IF   ${result}
            Set Global Variable    ${matching_set}    ${set}
            BREAK
        END
    END
    Should Not Be Empty    ${matching_set}
    Log    Set found: ${matching_set['name']}

Then user can see this is a "${expected_set_type}" set
    Should Be Equal As Strings    ${matching_set['set_type'].lower()}    ${expected_set_type.lower()}
    Log    The set type matches the expected value: ${expected_set_type}
