*** Settings ***
Resource    ../../../settings/settings.resource

*** Keywords ***
Given table does not exist in the database
    [Arguments]    ${table_name}
    ${output}=  Query    SHOW TABLES LIKE '%${table_name}%';
    Should Be Empty    ${output}    Table ${table_name} exist in the database

When table is created in the database
    [Arguments]    ${table_name}
    Execute Sql String    CREATE TABLE ${table_name} (ID INT AUTO_INCREMENT, username VARCHAR(255), email VARCHAR(255), password VARCHAR(255), PRIMARY KEY (ID));

Then table is created empty
    [Arguments]    ${table_name}
    Table Must Exist    ${table_name}
    Row Count is 0    SELECT * FROM ${table_name};

When data is added to the table
    [Arguments]    ${table_name}
    Execute SQL String    INSERT INTO ${table_name} (username, email, password) VALUES ('JohnDoe', 'john.doe@example.com', 'password123');

Then data should exist in the table
    [Arguments]    ${table_name}
    ${output}=  Query    SELECT * FROM ${table_name} WHERE username='JohnDoe';
    Should Not Be Empty    ${output}    Data for 'JohnDoe' not found in the database.

Given table exists in the database
    [Arguments]    ${table_name}
    Table Must Exist    ${table_name}

When table is deleted from the database
    [Arguments]    ${table_name}
    ${output}=  Query    DROP TABLE ${table_name};
    Should Be Equal As Strings  ${output}  []

Then table no longer exists in the database
    [Arguments]    ${table_name}
    ${show_tables_query}=    Set Variable    SHOW TABLES;
    ${tables}=    Query    ${show_tables_query}
    Should Not Contain    ${tables}    ${table_name}    Table still exists in the database after dropping.   

When data is updated in the database
    [Arguments]    ${table_name}
    Execute Sql String    UPDATE ${table_name} SET username='JaneDoe' WHERE ID = 1;

Then table is successfully updated with the new data
    [Arguments]    ${table_name}
    ${output}=  Query    SELECT * FROM ${table_name} WHERE username='JaneDoe';
    Should Not Be Empty    ${output}    Data for 'JaneDoe' not found in the database.

When data is deleted from the database
    [Arguments]    ${table_name}
    Execute Sql String    DELETE FROM ${table_name} WHERE ID = 1;

Then deleted data no longer exist in the database
    [Arguments]    ${table_name}
    Row Count is 0    SELECT * FROM ${table_name} WHERE username = 'JaneDoe'; 

When table is not empty
    [Arguments]    ${table_name}
    Row Count is Greater Than X    SELECT * FROM ${table_name};    0

Then data can be read from the table
    [Arguments]    ${table_name}
    Check If Exists In Database    SELECT id FROM ${table_name} WHERE username = 'JohnDoe'; 