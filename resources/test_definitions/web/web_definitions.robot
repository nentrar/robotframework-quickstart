*** Settings ***
Resource    ../../../settings/settings.resource

*** Keywords ***
Open Fullscreen Browser
    [Arguments]    ${browser_name} 
    Open Browser    about:blank    browser=${browser_name}
    Maximize Browser Window

Given user opens application main page
    Go To    ${BAZAAR_URL}
    ${title}=    Get Title
    Should Be Equal As Strings    ${title}    Magiczny Bazar

When user open logging page
    Wait Until Element Is Visible    //a[text()="Zaloguj"]
    Click Element    //a[text()="Zaloguj"]
    Wait Until Element Is Visible    //h5[text()="Login" and preceding-sibling::h2[text()="Magiczny Bazar"]]
    Element Should Be Visible    //h5[text()="Login" and preceding-sibling::h2[text()="Magiczny Bazar"]]

And user enter the email "${user_email}"
    Input Text    //input[@id="email"]    ${user_email}

And user enter the password "${user_password}"
    Input Text    //input[@id="pwd"]    ${user_password}

Then user click Login button
    Click Element    //button[@type="submit"]

And user successfully log into the application
    Wait Until Page Contains Element    //span[text()="${TEST_USER_LOGIN}"]
