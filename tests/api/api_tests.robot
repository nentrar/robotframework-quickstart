*** Settings ***
Resource       ../../settings/settings.resource
Resource       ../../resources/test_definitions/api/api_definitions.robot
Suite Setup    Create Session    my_session    ${BASE_URL}    verify=true

*** Test Cases ***
Test sets endpoint
    [Documentation]  This is a test for checking the sets endpoint in MTG API
    [Tags]    api
    Given API is available at "sets" endpoint
    When user searches for set "Fallout"
    Then user can see this is a "Commander" set