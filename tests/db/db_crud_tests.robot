*** Settings ***
Documentation   Basic CRUD operations on database
Test Tags       crud
Resource        ../../settings/settings.resource
Resource        ../../resources/test_definitions/db/db_definitions.robot
Suite Setup     Connect To Database    pymysql    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
Suite Teardown  Disconnect From Database

*** Test Cases ***
Table can be created in the database
    [Tags]    create_table
    Given table does not exist in the database    User
    When table is created in the database    User
    Then table is created empty    User

User data can be add to the database
    [Tags]    add_data
    Given table exists in the database    User
    When data is added to the table    User
    Then data should exist in the table    User

User data can be read from the database
    [Tags]    read_data
    Given table exists in the database    User
    When table is not empty    User
    Then data can be read from the table    User

User data can be update in the database
    [Tags]    update_data
    Given table exists in the database    User
    When data is updated in the database    User
    Then table is successfully updated with the new data    User

User data can be deleted from the database
    [Tags]    delete_data
    Given table exists in the database    User
    When data is deleted from the database    User
    Then deleted data no longer exist in the database    User

Existing table can be deleted from the database
    [Tags]    delete_table
    Given table exists in the database   User
    When table is deleted from the database    User
    Then table no longer exists in the database    User

    
