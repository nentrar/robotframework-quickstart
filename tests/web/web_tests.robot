*** Settings ***
Resource        ../../settings/settings.resource
Resource        ../../resources/test_definitions/web/web_definitions.robot
Suite Setup     Open Fullscreen Browser    safari
Suite Teardown  Close Browser

*** Test Cases ***
User can successfully log into application website
    [Tags]   web
    Given user opens application main page
    When user open logging page
    And user enter the email "${TEST_USER_EMAIL}"
    And user enter the password "${TEST_USER_PASSWORD}"
    Then user click Login button
    And user successfully log into the application
